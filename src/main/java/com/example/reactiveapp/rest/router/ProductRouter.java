package com.example.reactiveapp.rest.router;
import com.example.reactiveapp.rest.handler.ProductHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;


@Configuration
public class ProductRouter {
    @Bean
    public RouterFunction<ServerResponse> route(ProductHandler productHandler){
        return RouterFunctions.route(RequestPredicates.GET("/products")
                .and(RequestPredicates.accept(MediaType.APPLICATION_JSON)), productHandler::getProduct);
    }
}
