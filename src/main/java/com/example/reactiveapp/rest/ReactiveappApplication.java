package com.example.reactiveapp.rest;

import com.example.reactiveapp.rest.client.ProductWebClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveappApplication {
	public static void main(String[] args) {

		SpringApplication.run(ReactiveappApplication.class, args);
		ProductWebClient gwc = new ProductWebClient();
		System.out.println(gwc.getResult());
	}


}
